import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-left-nav-login',
  templateUrl: './left-nav-login.component.html',
  styleUrls: ['./left-nav-login.component.css']
})
export class LeftNavLoginComponent implements OnInit {

  constructor(private router: Router) { }
  ngOnInit() {

  }

  login() {
    this.router.navigate(['login']);
  }

  registration() {
    this.router.navigate(['registration']);
  }
}
