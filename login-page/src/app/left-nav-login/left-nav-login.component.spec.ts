import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftNavLoginComponent } from './left-nav-login.component';

describe('LeftNavLoginComponent', () => {
  let component: LeftNavLoginComponent;
  let fixture: ComponentFixture<LeftNavLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftNavLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftNavLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
