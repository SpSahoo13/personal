import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
   selector: 'app-login-page',
   templateUrl: './login-page.component.html',
   styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
   constructor(private router: Router) { }
   username: string;
   password: string;
   ngOnInit() {
   }
   login(): void {
      if (this.username === 'admin' && this.password === 'admin') {
         this.router.navigate(['registration']);
      } else {
         alert('Invalid credentials');
      }
   }
   registration() {
      this.router.navigate(['registration']);
    }
}

