import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { LeftNavLoginComponent } from './left-nav-login/left-nav-login.component';
import {LoginPageComponent} from  '../app/login-page/login-page.component';
import {FormsModule} from '@angular/forms';
import { RegistrationComponent } from './registration/registration.component';
import { UiSwitchModule } from 'ngx-toggle-switch';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    LeftNavLoginComponent,
    LoginPageComponent,
    RegistrationComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    UiSwitchModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
