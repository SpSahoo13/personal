import { RegistrationComponent } from './registration/registration.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginPageComponent} from '../app/login-page/login-page.component';
import { LeftNavLoginComponent } from './left-nav-login/left-nav-login.component';

const routes: Routes = [
  {
    path: 'login',
  component: LoginPageComponent},
  {
    path: '',
  component: LeftNavLoginComponent},
  {
    path: 'registration',
  component: RegistrationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
