import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeraComponentComponent } from './mera-component.component';

describe('MeraComponentComponent', () => {
  let component: MeraComponentComponent;
  let fixture: ComponentFixture<MeraComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeraComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeraComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
