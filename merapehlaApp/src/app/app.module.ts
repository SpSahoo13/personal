import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MeraComponentComponent } from './mera-component/mera-component.component';
import { ChangetextDirective } from './changetext.directive';

@NgModule({
  declarations: [
    AppComponent,
    MeraComponentComponent,
    ChangetextDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
