import { Component } from  '@angular/core';
import { Customer } from './CustomerApp.Model';

@Component({
 
  templateUrl: './CustomerApp.CustomerView.html'
  
})
export class CustomerComponent {
  title = 'CUSTOMER_APPLICATION';
  CustomerModel : Customer =new Customer();
  CustomerModels : Array<Customer> = new Array<Customer>(); 
  Add(){
    this.CustomerModels.push(this.CustomerModel);
    this.CustomerModel =new Customer(); // Clear Ui
  }
}
