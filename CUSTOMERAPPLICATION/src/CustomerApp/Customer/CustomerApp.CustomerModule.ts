
import { NgModule } from '@angular/core';
import {FormsModule}from '@angular/forms';
import {RouterModule} from '@angular/router';

import { CustomerRoutes } from '../Routing/CustomerApp.CustomerRouting';
import { CustomerComponent } from './CustomerApp.CustomerComponent';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [
    CustomerComponent
  ],
  imports: [
    RouterModule.forChild(CustomerRoutes),
    CommonModule,FormsModule
   
  ],
  providers: [],
  bootstrap: [CustomerComponent]
})
export class CustomerModule { }
