import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule}from '@angular/forms';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './CustomerApp.HomeComponent';
import { MasterComponent } from './CustomerApp.MasterComponent';
import { MainRoutes } from '../Routing/CustomerApp.MainRouting';



@NgModule({
  declarations: [
    HomeComponent,MasterComponent
  ],
  imports: [
    RouterModule.forRoot(MainRoutes),
    BrowserModule,FormsModule
   
  ],
  providers: [],
  bootstrap: [MasterComponent]
})
export class MainModule { }
