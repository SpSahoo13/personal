import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
  <h4>"ngIf Example"</h4>
  <h4 *ngIf="displayName ; then thenBlock; else elseBlock"></h4>
  <ng-template #thenBlock>
    <h4>Hi if-then Block</h4>
  </ng-template>

  <ng-template #elseBlock>
  <h4>Hi else Block</h4>
  </ng-template>

  <h4>"ngSwitch Example"</h4>
    <div [ngSwitch] = "colour"> 
      <div *ngSwitchCase="'red'"> You choose Red</div>
      <div *ngSwitchCase="'green'"> You choose Green</div>
      <div *ngSwitchCase="'blue'"> You choose Blue</div>
      <div *ngSwitchDefault>Pick another again</div>
    </div>

    <h4>"ngFor Example"</h4>
      <div *ngFor="let color of colors ; index as i; first as f; last as l; odd as o; even as e">
      <h4>{{i }}{{color}}{{f}} {{o}} {{e}}</h4>
      </div>
  `,
  styles: [``]
})
export class AppComponent {

  public colors = ["red","green","blue","yellow"];
  displayName=true;
  public colour="  ";
  
  title = 'atructural-directives';

}
