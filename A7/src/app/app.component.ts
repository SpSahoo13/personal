import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
<h4>"DATA BINDING TYPES" <br> "1. Interpolation Examples" </h4>
  <h4>Hello {{name}} !!!<h4>
  <h4>{{name.toLowerCase()}} </h4>
  <h4>{{greetUser()}}</h4>
"2. Property Binding Examples"<br><br>
  <input [id]="myId" type="text" value = "binding myId value to the html id property"><br>
  <input id={{myId}} type="text" value = "using interpolation to bind to the id of the element"><br>
  <input [disabled]="isDisabled" type="text" value = "somu"><br>
  <input bind-disabled="isDisabled" type="text" value = "alternate to [ ] ">
<h4>"3. Class Binding Examples"</h4>
  <h4 class="success">regular way to apply class to this element</h4>
  <h4 [class]="successClass">Classbinding</h4>
  <h4 [class.danger]="hasError">Apply class based on expression that evaluates to be true/false</h4>
  <h4 [ngClass]="msgcls">example of ngclass</h4>
<h4 [style.color]="hasError ? 'blue' : 'pink'">"4. Style Binding Examples"</h4>
  <h4 [style.color] = "highLightColor">Alternate_way</h4>
  <h4 [ngStyle]="titleStyles">ngStyle directive</h4>
<h4>"EVENT BINDING"</h4>
  <button (click)= "onClick()">Greet</button>
  {{greeting}}
  <button (click)= "greeting = 'Welcome Som'">Greet</button>
<h4>Template Reference Variable</h4>
 <input #myinput type="text" >
 <button (click)="onClick2(myinput.value)">Log</button>  
 {{greeting2}}
 <h4>Two way Binding</h4>
 <input [(ngModel)]="names" type="text">
 {{names}}
  `,
  styles: [`
  .success{
    color : green;
  }
  .danger {
    color : red;
  }
  .special{
    font-style : italic;
  }
  
  `]
})
export class AppComponent {
 public name = "som" ;
 public names = "" ;
public myId = "testid";
public isDisabled=true;
public successClass = "success"
public hasError = true;
public isSpecial = true;
public highLightColor = "brown";
public msgcls={
 "success" : !this.hasError,
 "danger" : this.hasError,
 "special" : this.isSpecial 
}
public titleStyles = {
  color : "blue",
  fontStyle : "italic"
}
 greetUser() {
  return "Hello " + this.name;
}
  onClick(){
    console.log('Welcome Som to console!!!')
    this.greeting="Welcome to page display"
  }
  public greeting= "";
  public greeting2= "";
  
  onClick2(value){
    console.log('welcome ' + value)
    this.greeting2= "welcome to page " + value
  }
}
